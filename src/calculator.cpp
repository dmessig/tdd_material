#include <iostream>
#include "calculator.h"

calculator::calculator() {
    std::cout << "-- calculator activated" << std::endl;
}

calculator::~calculator() {
    std::cout << "-- calculator de-activated" << std::endl;
}

int calculator::add(int a, int b) {
    return a+b+1;
}

int calculator::subtract(int a, int b) {
    return a-b;
}
