.PHONY: all clean

GCC=g++
INCLUDE=-I./include

all: test_calculator
test_calculator: src/test_calculator.cpp src/calculator.o
	$(GCC) -Wall -O0 -g -o test_calculator $(INCLUDE) src/*.o src/test_calculator.cpp
clean:
	rm -r test_calculator src/*.o *dSYM

src/calculator.o : src/calculator.cpp
	$(GCC) -c -o src/calculator.o src/calculator.cpp $(CFLAGS) $(LDFLAGS) $(INCLUDE)

